# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a solution for a Backend Developer Challenge
* Functionalities: login, add insurance, edit insurance title and delete insurance

### How do I get set up? ###

* Create .env file according to the .env.example
* .env should be:
    SESSION_SECRET=some_session_secret!
    SECRET_KEY=some_secret_key!

    DB_USER=brfvuusgmilely
    DB_PASSWORD=711df2db838f4fb2c2e373754eeb395d81e1906c4717411efa635402b96fb3fe
    DB_DATABASE=d7ahckfjuvf68v
    DB_HOST=ec2-50-16-196-138.compute-1.amazonaws.com
    DB_PORT=5432
* npm i
* npm run build:watch
* for logging in use following credentials:
    email: creadi@gmail.com
    password: creadi123

### API documentation ###

* POST "/v1/signup"
    - user journey
    - req.body:
        email: string
        password: string
        confirmPassword: string

* POST "/v1/login"
    - user login
    - req.body:
        email: string
        password: string
        
* POST "/v1/insurance"
    - add insurance 
    - Authorization Bearer <TOKEN>
    - req.body:
        title: string
        company: string
        price: number
        
* GET "/v1/insurances"
    - returns all insurances
    - Authorization Bearer <TOKEN>
        
* PUT "/v1/insurance/edit"
    - edit insurance title
    - Authorization Bearer <TOKEN>
    - req.body:
        title: string
        insuranceId: number
        
* DELETE "/v1/insurance/delete"
    - delete insurance 
    - Authorization Bearer <TOKEN>
    - req.body:
        insuranceId: number

### How to run tests? ###

* npm i (if not already)
* npm run test

### Logs ###

* All logs can be found in logs.log in root folder