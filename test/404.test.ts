
import { expect } from "chai";
import app from "../src/app";
import * as supertest from "supertest";

const request = supertest(app);

describe("GET /random-url", () => {

    it("should return 404", () => {
      return request.get("/random-url")
        .expect(404);
    });

});
