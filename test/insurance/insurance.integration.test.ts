
import * as testData from "../test-data";
import * as supertest from "supertest";
import app from "../../src/app";


describe("POST /v1/insurance", () => {
    let request;
    let token;

    beforeAll(async function(done) {
        request = supertest(app);
        request
            .post("/v1/login")
            .send({
                email: testData.userData.email,
                password: testData.userData.password
            })
            .end(function(err, res) {
                token = res.body.token;
                done();
            });
    });

    it("All ok. Expect 200.", async () => {
        request
            .post("/v1/insurance")
            .set("Authorization", "bearer " + token)
            .send({
                title: testData.newInsuranceData.title,
                company: testData.newInsuranceData.company,
                price: testData.newInsuranceData.price
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Wrong parameters. Expect 400.", async () => {
        return request
            .post("/v1/insurance")
            .set("Authorization", "bearer " + token)
            .send({
                title: 1,
                company: testData.newInsuranceData.company,
                price: testData.newInsuranceData.price
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
    });

    it("No authorization. Expect 401.", async () => {
        return request
            .post("/v1/insurance")
            .set("Authorization", "bearer " + "")
            .send({
                title: testData.newInsuranceData.title,
                company: testData.newInsuranceData.company,
                price: testData.newInsuranceData.price
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(401);
    });

});


describe("GET /v1/insurances", () => {
    let request;
    let token;

    beforeAll(async function(done) {
        request = supertest(app);
        request
            .post("/v1/login")
            .send({
                email: testData.userData.email,
                password: testData.userData.password
            })
            .end(function(err, res) {
                token = res.body.token;
                done();
            });
    });

    it("All ok. Expect 200.", async () => {
        request
            .get("/v1/insurances")
            .set("Authorization", "bearer " + token)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("No authorization. Expect 401.", async () => {
        return request
            .get("/v1/insurances")
            .set("Authorization", "bearer " + "")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(401);
    });

});


describe("PUT /v1/insurance", () => {
    let request;
    let token;

    beforeAll(async function(done) {
        request = supertest(app);
        request
            .post("/v1/login")
            .send({
                email: testData.userData.email,
                password: testData.userData.password
            })
            .end(function(err, res) {
                token = res.body.token;
                done();
            });
    });

    it("All ok. Expect 200.", async () => {
        request
            .put("/v1/insurance")
            .set("Authorization", "bearer " + token)
            .send({
                title: testData.newInsuranceData.title,
                insuranceId: 1
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Wrong parameters. Expect 400.", async () => {
        return request
            .put("/v1/insurance")
            .set("Authorization", "bearer " + token)
            .send({
                title: 1,
                insuranceId: 1
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
    });

    it("No authorization. Expect 401.", async () => {
        return request
            .put("/v1/insurance")
            .set("Authorization", "bearer " + "")
            .send({
                title: testData.newInsuranceData.title,
                insuranceId: 1
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(401);
    });

});


describe("DELETE /v1/insurance", () => {
    let request;
    let token;

    beforeAll(async function(done) {
        request = supertest(app);
        request
            .post("/v1/login")
            .send({
                email: testData.userData.email,
                password: testData.userData.password
            })
            .end(function(err, res) {
                token = res.body.token;
                done();
            });
    });

    it("All ok. Expect 200.", async () => {
        request
            .delete("/v1/insurance")
            .set("Authorization", "bearer " + token)
            .send({
                insuranceId: 1
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Wrong parameters. Expect 400.", async () => {
        return request
            .delete("/v1/insurance")
            .set("Authorization", "bearer " + token)
            .send({
                insuranceId: "wrong"
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
    });

    it("No authorization. Expect 401.", async () => {
        return request
            .delete("/v1/insurance")
            .set("Authorization", "bearer " + "")
            .send({
                insuranceId: 1
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(401);
    });

});