
import { insuranceService } from "./../../src/components/insurance";
import { expect } from "chai";
import * as testData from "../test-data";

describe("Testing insurance component", () => {

    it("Should get insurances", async () => {
        const insurances = await insuranceService.getInsurances(testData.userData.id);
        expect(insurances).is.an("array");
    });

    it("Should add insurance", async () => {
        await insuranceService.addInsurance(testData.newInsuranceData.title, testData.newInsuranceData.company, testData.newInsuranceData.price, testData.userData.id);
        const insurances = await insuranceService.getInsurances(testData.userData.id);
        expect(insurances).is.an("array");
    });

    it("Should check if insurance exists", async () => {
        const insurances = await insuranceService.checkInsuranceExists(0);
        expect(insurances).to.equal(false);
    });

    it("Should check if insurance exists", async () => {
        const insurances = await insuranceService.checkInsuranceExists(1);
        expect(insurances).to.equal(true);
    });

    it("Should rename insurance title", async () => {
        await insuranceService.editInsurance("new title", 2);
        const insurances = await insuranceService.getInsurances(testData.userData.id);
        expect(insurances[1]).is.an("object").to.have.property("title");
    });

    it("Should archive insurance", async () => {
        await insuranceService.archiveInsurance(2);
        const insurances = await insuranceService.getInsurances(testData.userData.id);
        expect(insurances).is.an("array");
    });

});

