
import { userService } from "./../../src/components/user/user.service";
import { expect } from "chai";
import * as testData from "../test-data";

describe("Testing user component", () => {

    it("Should post user", async () => {
        const user = await userService.postUser(testData.userData.email, testData.userData.password);
        expect(user).to.equal(true);
    });

    it("Should get user", async () => {
        const user = await userService.getUsers(testData.userData.email, testData.userData.password);
        expect(user[0]).is.an("object").that.contain.keys("id", "user");
    });

    it("Should check if user exists", async () => {
        const user = await userService.checkUserExists(testData.userData.email);
        expect(user).to.equal(true);
    });

    it("Should check if does not user exist", async () => {
        const user = await userService.checkUserExists("a@b.c");
        expect(user).to.equal(false);
    });

});