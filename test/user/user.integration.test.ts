
import { userService } from "./../../src/components/user/user.service";
import { expect } from "chai";
import * as testData from "../test-data";
import * as supertest from "supertest";
import app from "../../src/app";

const request = supertest(app);

describe("POST /v1/login", () => {

    it("All ok. Expect 200.", async () => {
        return request.post("/v1/login")
            .send({
                email: testData.userData.email,
                password: testData.userData.password
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
    });

    it("Wrong parameters. Expect 400.", async () => {
        return request.post("/v1/login")
            .send({
                email: "notEmail",
                password: testData.userData.password
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400);
    });

    it("No user. Expect 404.", async () => {
        return request.post("/v1/login")
            .send({
                email: "somebody@somebody.com",
                password: testData.userData.password
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404);
    });

});
