
import { userService } from "./user.service";
import { Router, Request, Response } from "express";
import { catchErrors, resError, errorsLib } from "../../utils";
import { signupValidation, loginValidation } from "../../middleware";
import * as jwt from "jsonwebtoken";
import { isEmpty } from "lodash";
import * as dotenv from "dotenv";

dotenv.config({ path: ".env" });

const router = Router();

const secretKey = process.env.SESSION_SECRET;

router.get("/",
    catchErrors(async (req: Request, res: Response) => {
        return res.status(200).json({msg: "Hello Creadi"});
    })
);

router.post("/signup", signupValidation,
    catchErrors(async (req: Request, res: Response) => {

        const { email, password } = req.body;

        const userExists: Boolean = await userService.checkUserExists(email);
        if (userExists) return resError(errorsLib.userExists);

        const queryResult: Boolean = await userService.postUser(email, password);
        if (!queryResult) return resError(errorsLib.databaseNotResponding);

        return res.status(200).json("Customer added successfully");
    })
);

router.post("/login", loginValidation,
    catchErrors(async (req: Request, res: Response) => {

        const { email, password } = req.body;

        const users: UsersData = await userService.getUsers(email, password);
        if (isEmpty(res) || !users.length) return resError(errorsLib.userNotFound);

        jwt.sign({users}, secretKey, (err: any, token: any) => {
            if (err) return res.json({message: err.message});
            return res.status(200).json({token});
        });

    })
);

export const userController = router;
