
import { userRepository } from "./user.repository";
import { catchErrors, resError, errorsLib } from "../../utils";
import { isEmpty } from "lodash";
import { QueryResult } from "pg";

export class UserService {

    public async postUser (email: string, password: string): Promise<Boolean> {
        const res: QueryResult = await userRepository.postUser(email, password);

        if (isEmpty(res) || res.rowCount === 0) {
            return false;
        }

        return true;
    }

    public async checkUserExists (email: string): Promise<Boolean> {
        const res: QueryResult = await userRepository.checkUserExists(email);

        if (isEmpty(res) || !res.rows.length) {
            return false;
        }

        return true;
    }

    public async getUsers (email: string, password: string): Promise<UsersData> {
        const res: QueryResult = await userRepository.getUsers(email, password);
        return res.rows;
    }

}

export const userService = new UserService();