
import { client } from "../../common/database";
import { QueryResult } from "pg";

export class UserRepository {

    public async getUsers (email: string, password: string): Promise<QueryResult> {
        const query =
            `SELECT id, user
            FROM users
            WHERE email='${email}' AND password='${password}'`;
        return await client.query(query);
    }

    public async checkUserExists (email: string): Promise<QueryResult> {
        const query =
            `SELECT *
            FROM users
            WHERE email='${email}'`;
        return await client.query(query);
    }

    public async postUser (email: string, password: string): Promise<QueryResult> {
        const query =
            `INSERT INTO users (email, password)
            VALUES ('${email}', '${password}')`;
        return await client.query(query);
    }

}

export const userRepository = new UserRepository();