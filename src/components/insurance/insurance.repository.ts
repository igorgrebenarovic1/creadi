
import { client } from "../../common/database";
import { QueryResult } from "pg";

export class InsuranceRepository {

    public async getInsurances (id: number): Promise<QueryResult> {

        const query =
            `SELECT *
            FROM insurances
            WHERE user_fk='${id}' AND archived=0`;

        return await client.query(query);

    }

    public async getInsurance (id: number): Promise<QueryResult> {

        const query =
            `SELECT *
            FROM insurances
            WHERE id='${id}'`;

        return await client.query(query);

    }

    public async addInsurance(title: string, company: string, price: number, id: number): Promise<QueryResult> {

        const query =
            `INSERT INTO insurances (title, company, price, archived, user_fk)
            VALUES ('${title}', '${company}', ${price}, 0, ${id})`;

        return await client.query(query);

    }

    public async editInsurance(title: string, id: number): Promise<QueryResult> {
        const query =
            `UPDATE insurances
            SET title = '${title}'
            WHERE id = ${id};`;

        return await client.query(query);
    }

    public async archiveInsurance(id: number): Promise<QueryResult> {
        const query =
            `UPDATE insurances
            SET archived = 1
            WHERE id = ${id};`;

        return await client.query(query);
    }


}

export const insuranceRepository = new InsuranceRepository();