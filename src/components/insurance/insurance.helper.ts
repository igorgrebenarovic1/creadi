
export function escapeCharacters(str: string): string {
    return str.replace(/[']/g, "''");
}