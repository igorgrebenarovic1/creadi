
import { Router, Request, Response } from "express";
import { catchErrors, resError, errorsLib } from "../../utils";
import { verifyToken, detectUser, newInsuranceValidation, editInsuranceValidation, deleteInsuranceValidation } from "../../middleware";
import logger from "../../utils/logger";
import { isEmpty } from "lodash";
import { insuranceService } from ".";

const router = Router();

router.post("/insurance", verifyToken, detectUser, newInsuranceValidation,
    catchErrors(async (req: Request, res: Response, authData: any) => {

        const id = req.id;
        const { title, company, price } = req.body;

        const queryResult: Boolean = await insuranceService.addInsurance(title, company, price, id);
        if (!queryResult) return resError(errorsLib.databaseNotResponding);

        logger.log("info", `Insurance for user ${id} added!`);
        res.status(200).json("Insurance added");

    })
);


router.get("/insurances", verifyToken, detectUser,
    catchErrors(async (req: Request, res: Response) => {

        const id = req.id;

        logger.log("info", `Getting insurances for user ${id}...`);

        const insurances: InsurancesData = await insuranceService.getInsurances(id);
        if (isEmpty(insurances) || !insurances.length) return resError(errorsLib.insuranceNotFound);

        logger.log("info", `Insurances for user ${id} found!`);
        res.status(200).json(insurances);

    })
);

router.put("/insurance", verifyToken, detectUser, editInsuranceValidation,
    catchErrors(async (req: Request, res: Response) => {

        const { title, insuranceId } = req.body;

        logger.log("info", `Editing insurances (${insuranceId}) title...`);

        const insuranceExists: Boolean = await insuranceService.checkInsuranceExists(insuranceId);
        if (!insuranceExists) return resError(errorsLib.insuranceNotFound);

        const queryResult: Boolean = await insuranceService.editInsurance(title, insuranceId);
        if (!queryResult) return resError(errorsLib.databaseNotResponding);

        logger.log("info", `Insurance (${insuranceId}) title edited!`);
        res.status(200).json("Insurance title edited");

    })
);

router.delete("/insurance", verifyToken, detectUser, deleteInsuranceValidation,
    catchErrors(async (req: Request, res: Response) => {

        const { insuranceId } = req.body;

        logger.log("info", `Deleting insurances (${insuranceId})...`);

        const insuranceExists: Boolean = await insuranceService.checkInsuranceExists(insuranceId);
        if (!insuranceExists) return resError(errorsLib.insuranceNotFound);

        const queryResult: Boolean = await insuranceService.archiveInsurance(insuranceId);

        if (!queryResult) return resError(errorsLib.databaseNotResponding);

        logger.log("info", `Insurance (${insuranceId}) deleted!`);
        res.status(200).json("Insurance deleted");

    })
);


export const insuranceController = router;