
import { insuranceRepository } from ".";
import { catchErrors, resError, errorsLib } from "../../utils";
import { QueryResult } from "pg";
import { isEmpty } from "lodash";
import { escapeCharacters } from ".";

export class InsuranceService {

    public async addInsurance(title: string, company: string, price: number, id: number): Promise<Boolean> {

        title = escapeCharacters(title);
        company = escapeCharacters(company);

        const res: QueryResult = await insuranceRepository.addInsurance(title, company, price, id);

        if (isEmpty(res) || res.rowCount === 0) {
            return false;
        }

        return true;
    }

    public async getInsurances(id: number): Promise<InsurancesData> {
        const res: QueryResult = await insuranceRepository.getInsurances(id);
        return res.rows;
    }

    public async checkInsuranceExists(id: number): Promise<Boolean> {
        const res: QueryResult = await insuranceRepository.getInsurance(id);

        if (isEmpty(res) || !res.rows.length) {
            return false;
        }

        return true;
    }

    public async editInsurance(title: string, id: number): Promise<Boolean> {

        title = escapeCharacters(title);

        const res: QueryResult = await insuranceRepository.editInsurance(title, id);

        if (isEmpty(res) || res.rowCount === 0) {
            return false;
        }

        return true;
    }

    public async archiveInsurance(id: number): Promise<Boolean> {
        const res: QueryResult = await insuranceRepository.archiveInsurance(id);

        if (isEmpty(res) || res.rowCount === 0) {
            return false;
        }

        return true;
    }

}

export const insuranceService = new InsuranceService();