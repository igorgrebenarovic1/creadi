interface InsuranceData {
    "id": number;
    "title": string;
    "company": string;
    "price": number;
    "archived": number;
    "user_fk": number;
}

interface InsurancesData extends Array<InsuranceData> {}