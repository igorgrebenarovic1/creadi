interface UserData {
    id: number;
    token: string;
}

interface UsersData extends Array<UserData> {}