declare namespace Express {
    export interface Request {
        token: string;
        id: number;
    }
 }