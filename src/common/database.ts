
import * as dotenv from "dotenv";
import * as pg from "pg";

dotenv.config({ path: ".env" });

export const client = new pg.Client({
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: +process.env.DB_PORT,
    host: process.env.DB_HOST,
    ssl: true
});

client.connect();
