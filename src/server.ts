
import * as http from "http";
import app from "./app";
import logger from "./utils/logger";

const port = process.env.PORT || 3000;

http
    .createServer(app)
    .listen(port, () => logger.log("info", `App is running at http://localhost:${port}`));