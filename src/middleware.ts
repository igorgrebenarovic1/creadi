
import { Router, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import * as Joi from "joi";
import { resError, errorsLib, catchErrors } from "./utils";

import * as dotenv from "dotenv";
import { nextTick } from "async";
import logger from "./utils/logger";
dotenv.config({ path: ".env" });


const secretKey = process.env.SESSION_SECRET;

export const verifyToken = (req: Request, res: Response, next: any) => {

    logger.log("info", `Veryfing token...`);

    const bearerHeader = req.headers.authorization;

    if (!bearerHeader) {
        logger.log("info", "Authorization token missing");
        return res.status(401).json("Authorization needed");
    }

    req.token = String(bearerHeader).split(" ")[1];

    logger.log("info", `Token veryfied!`);

    next();
};

export const detectUser = async (req: Request, res: Response, next: any) => {

    logger.log("info", `Detecting user...`);

    await jwt.verify(req.token, secretKey, async (err: Error, authData: any) => {

        if (err) {
            logger.log("info", "User not authorized");
            return res.status(401).json("Unauthorized");
        }

        req.id = authData.users[0].id;

        logger.log("info", `User detected!`);

        next();
    });
};

export const newInsuranceValidation = (req: Request, res: Response, next: any) => {

    logger.log("info", `Adding new insurance...`);

    const { title, company, price } = req.body;

    const schema = Joi.object().keys({
        title: Joi.string().required(),
        company: Joi.string().required(),
        price: Joi.number().required()
    });

    const { error } = Joi.validate({ title, company, price }, schema);

    if (error) {
        logger.log("info", `Wrong insurance parameters.`);
        return res.status(400).json("Wrong parameters");
    }

    next();
};

export const signupValidation = (req: Request, res: Response, next: any) => {
    const { email, password, confirmPassword } = req.body;

    if (password !== confirmPassword) {
        return res.status(400).json("Passwords must match");
    }

    const schema = Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{4,30}$/).required(),
        confirmPassword: Joi.string().regex(/^[a-zA-Z0-9]{4,30}$/).required()
    });

    const { error } = Joi.validate({ email, password, confirmPassword }, schema);

    if (error) {
        return res.status(400).json("Wrong parameters");
    }

    next();
};

export const loginValidation = (req: Request, res: Response, next: any) => {

    const { email, password } = req.body;

    const schema = Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{4,30}$/).required()
    });

    const { error } = Joi.validate({ email, password }, schema);

    if (error) {
        return res.status(400).json("Wrong parameters");
    }

    next();
};

export const editInsuranceValidation = (req: Request, res: Response, next: any) => {

    logger.log("info", `Editing insurance title...`);

    const { title, insuranceId } = req.body;

    const schema = Joi.object().keys({
        title: Joi.string().required(),
        insuranceId: Joi.number().required()
    });

    const { error } = Joi.validate({ title, insuranceId }, schema);

    if (error) {
        logger.log("info", `Wrong insurance parameters.`);
        return res.status(400).json("Wrong parameters");
    }

    next();
};

export const deleteInsuranceValidation = (req: Request, res: Response, next: any) => {

    logger.log("info", `Deleting insurance...`);

    const { insuranceId } = req.body;

    const schema = Joi.object().keys({
        insuranceId: Joi.number().required()
    });

    const { error } = Joi.validate({ insuranceId }, schema);

    if (error) {
        logger.log("info", `Wrong insurance parameters.`);
        return res.status(400).json("Wrong parameters");
    }

    next();
};
