import { Request, Response, NextFunction } from "express";
import logger from "./logger";

export interface ResponseError {
    status: number;
    message: string;
}

export interface ErrorLibrary {
    [field: string]: ResponseError;
}

export const errorsLib: ErrorLibrary = {
    unauthorized: {
        status: 401,
        message: "No authorization"
    },
    databaseNotResponding: {
        status: 503,
        message: "Database not responding"
    },
    userExists: {
        status: 409,
        message: "User under this email already exists"
    },
    insuranceNotFound: {
        status: 404,
        message: "Insurance not found"
    },
    userNotFound: {
        status: 404,
        message: "User not found"
    }
};

export const resError = (err: ResponseError, isThrow: boolean = true) => {
    const resErr: any = new Error();
    resErr.message = err.message;
    resErr.status = err.status;
    if (isThrow) {
        throw resErr;
    }
    return resErr;
};

export const catchErrors = (fn: Function) => (req: Request, res: Response, next: NextFunction) => {
  return Promise.resolve(fn(req, res, next))
    .catch((err: any) => {
        logger.log("error", `${err.status || 500}. ${err.errors || err.message}`);
        return res.status(err.status || 500).json({error: err.errors || err.message});
    });
};