import { Express, Request, Response, NextFunction } from "express";
import * as express from "express";
import logger from "./utils/logger";
import * as compression from "compression";
import * as session from "express-session";
import * as dotenv from "dotenv";
import * as bodyParser from "body-parser";
import * as errorHandler from "errorhandler";
import * as path from "path";
import { userController, insuranceController } from "./components";

dotenv.config({ path: ".env" });

const app = express();

process.on("unhandledRejection", (err: Error) => {
    logger.log("error", err.message);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

  app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: process.env.SESSION_SECRET
  }));

app.use(
    "/v1",
    userController,
    insuranceController
);

app.use(errorHandler());

export default app;